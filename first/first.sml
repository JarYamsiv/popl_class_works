fun head (x::xs) =x;
fun length (x::xs) = 1 + length xs
  | length []      = 0;


fun lengthWWC (x::xs) = 1 + lengthWWC xs
  | lengthWWC _       = 0;

fun isEmpty (_::_) = false
  | isEmpty _      = true;

fun sumList (x::xs) = x + sumList xs
  | sumList []      = 0;

fun map f (x::xs) = f x::map f xs
  | map f []      = [];

fun double x=2*x;



