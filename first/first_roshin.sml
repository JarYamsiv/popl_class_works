(*the function which maps one array to another using function f*)
fun map f (x::xs)= f x::map f xs
  | map f [] = [];

(* re turns first but without accepting a tuple*)
fun fst x y=x;

(*returns first by accepting a tuple*)
fun first (x,y) = x;

fun second (x,y) = y;

fun scnd x y=y;

(*creating a 3-tuple *)
val x = (1,"mewow",true);

(*to get the first value of any n tuple similiarly #2 #3 all are available*)
#1 x;

(*all the following has same behaviour known as curried form*)
fun plus1 x y = x+y;
fun plus2 x = fn y=> x+y;
val plus3 =fn x => fn y => x+y;

(*a single variable function in curried form*)
val increament = fn x => x+1;

(*usage of curried functions*)
val myIncr = plus1 1;


(*
a->b->c->d should be read as (a->(b->(c->d)))

*)

fun curryToUncurry f x y= f (x,y);


