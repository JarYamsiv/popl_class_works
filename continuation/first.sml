open SMLofNJ.Cont;

val a1= 3 + (callcc(fn k=> 2));
(*the k you get inside k is 3*)

val a2 = 3 + callcc (fn k=> throw k 10);

fun foo k = throw k 10 + 7;
(*foo a continuation expectes an integer*)

(*3+_ is a continuation which expectes an integer which 
foo suplies in the next setp*)

fun foo1 k = throw k 20 * 8;

val a3 = 3 - callcc foo1;
val a4 = 4 * callcc foo;

fun foo2 k = (throw k 10;7);

(*
	val throw : 'a cont -> 'a -> 'b
	the first i/p to throw is a continuation in this case it is 3+_
	and next one is the thing to be passed to the continuation
	throw doesnt return anything that's wh it's return type is 'b and not 'a
*)

val a4 = 3 + callcc foo2;

(*two *)
val somefun = isolate;
val k = isolate (fn x => print(Int.toString x) );

(*
throw k 5; this will get stuck try this ou in terminal
foo k; this will also get stuck
*)

val printInt = print o Int.toString;
val printK = isolate printInt;
val icRef = ref printK;	

fun save x = callcc (fn k=> (icRef := k;x));

fun save x = callcc (fn k=> (icRef := isolate (fn u => throw k u); x))