import requests, webbrowser ,bs4

print('searching...')
res = requests.get('https://en.wikipedia.org/wiki/List_of_National_Highways_in_India_by_old_highway_number')
res.raise_for_status()

soup = bs4.BeautifulSoup(res.text,"lxml")
outF = open("routes.pl", "w")

#table = soup.select('.wikitable sortable jquery-tablesorter table')
'''table = soup.find('table', attrs={'class':'wikitable sortable jquery-tablesorter'})
table_body = table.find('tbody')

rows = table_body.find_all('tr')
for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    data.append([ele for ele in cols if ele])'''
for table_row in soup.select("table.wikitable tr"):
	celles = table_row.findAll('td')
	route=""
	for i in range(len(celles)):
		if i==0:
			route+=("route(NH"+celles[i].text.rstrip()+",[")
			#print("\tc(",i,")",celles[i].text)
		if i==1:
			#print("\tc(",i,")",celles[i].text)
			name_list = (celles[i].text).split('-')
			for x in name_list:
				route+=x.rstrip()
				route+=","
			route.rstrip()
			route=route[:-1]
			route+="])."
	print(route)
	outF.write((route+'\n'))
