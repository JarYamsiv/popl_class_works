
route(NH1,[Delhi, Ambala, Jalandhar, Amritsar, Indo,Pak Border]).
route(NH1A,[Jalandhar, Madhopur, Jammu, Udhampur, Banihal, Srinagar, Baramulla, Uri]).
route(NH1B,[Batote, Doda, Kistwar, Symthan pass, Khanbal]).
route(NH1C,[Domel, Katra]).
route(NH1D,[Srinagar, Kargil, Leh]).
route(NH2,[Delhi, Mathura, Agra, Etawah, Auraiya, Akbarpur, Sachendi, Panki, Kanpur, Chakeri, Allahabad, Varanasi, Mohania, Barhi, Dhanbad, Asansol, Palsit, Dankuni]).
route(NH2A,[Sikandra, Bhognipur]).
route(NH2B,[Bardhaman, Bolpur]).
route(NH2B Ext,[The highways starting from Bolpur connecting Prantik,
Mayureswar and terminating at Mollarpur at the junction of
NH,60]).
route(NH2C,[Dehri, Akbarpur, Jadunathpur, Bihar/UP Border]).
route(NH3,[Agra, Dholpur, Morena, Gwalior, Shivpuri, Guna,_India, Indore, Dhule, Nashik, Thane, Mumbai]).
route(NH3A,[Dholpur, Bharatpur]).
route(NH4,[Junction with NH 3 near Thane, Pune, Kolhapur, Belgaum, Hubli, Davangere, Chitradurga, Tumkur, Bangalore, Kolar, Chittoor, Ranipet, Walajapet, Chennai]).
route(NH4A,[Belgaum, Anmod, Ponda, Panaji]).
route(NH4B,[Jawahar Lal Nehru Port Trust near KM 109, Palspe]).
route(NH4C,[NH,4 near Kalamboli at KM 116 junction with NH,4B near KM 16.687]).
route(NH5,[Junction with NH 6 near Baharagora,Baripada, Cuttack, Bhubaneswar, Visakhapatnam, Vijayawada, Guntur, Ongole, Nellore, Gummidipoondi, Chennai]).
route(NH5A,[Junction with NH 5 near Haridaspur, Paradip Port]).
route(NH6,[Hazira, Surat, Dhule, Jalgaon, Akola, Amravati, Nagpur, Durg, Raipur, Sambalpur, Baharagora, Kharagpur, Howrah, Kolkata]).
route(NH7,[Varanasi, Mangawan, Rewa, Jabalpur, Lakhnadon, Nagpur, Hyderabad, Kurnool, Chikkaballapur, Bangalore, Hosur, Krishnagiri, Dharmapuri, Salem, Namakkal, Karur, Dindigul, Madurai, Virudunagar, Tirunelveli, Kanyakumari]).
route(NH7A,[Palayamkottai, Tuticorin Port]).
route(NH8,[Delhi, Jaipur, Ajmer, Udaipur, Ahmedabad, Vadodara, Surat, Mumbai]).
route(NH8A,[Ahmedabad, Limbdi, Morvi, Kandla, Mandvi, Vikhadi, Kothra, Naliya, Narayan Sarover]).
route(NH8B,[Bamanbore, Rajkot, Porbunder]).
route(NH8C,[Chiloda, Gandhinagar, Sarkhej]).
route(NH8D,[Jetpur, Somnath]).
route(NH8E,[Bhavnagar, Somnath, Porbunder, Dwarka]).
route(NHNEI,[Ahmedabad, Vadodara Expressway]).
route(NH9,[Pune, Solapur, Hyderabad,Suryapet,Vijayawada, Machillipatnam]).
route(NH10,[Delhi, Rohtak, Hissar, Fatehabad, Sirsa, Fazilka, Indo,Pak Border]).
route(NH11,[Agra, Jaipur, Bikaner]).
route(NH11A,[Manoharpur, Dausa, Lalsot, Kothum]).
route(NH11B,[Lalsot, Karauli, Dholpur]).
route(NH11C,[Old alignment of NH no. 8 passing through Jaipur from km 220 to 273.50]).
route(NH12,[Jabalpur, Bhopal, Khilchipur, Aklera, Jhalawar, Kota, Bundi, Devli, Tonk, Jaipur]).
route(NH12A,[Jhansi, Jabalpur, Mandla, Chilpi, Simga near Raipur]).
route(NH13,[Solapur, Bijapur, Hospet, Chitradurga, Shimoga, Mangalore]).
route(NH14,[Beawar, Sirohi, Radhanpur]).
route(NH15,[Pathankot, Amritsar, Tarn Taran Sahib, Bhatinda, Ganganagar, Bikaner, Jaisalmer, Barmer, Samakhiali]).
route(NH16,[Nizamabad, Jagtial, Mancherial, Chinnur, Jagdalpur]).
route(NH17,[Panvel, Pen, Nagothana, Indapur, Mangaon, Mahad, Poladpur, Khed, Chiplun, Sangameshwar, Ratnagiri, Lanja, Rajapur, Kharepatan, Vaibhavwadi, Kankavli, Kudal, Sawantwadi, Pernem, Mapusa, Panaji, Karwar, Udupi, Suratkal, Mangalore, Kannur, Kozhikode, Ferokh, Kottakkal,Kuttippuram, Ponnani, Chavakkad, North Paravur Junction with NH 47 near Edapally at Kochi]).
route(NH17A,[Junction with NH 17 near Cortalim, Murmugao]).
route(NH17B,[Ponda, Verna, Vasco]).
route(NH18,[Junction with NH 7 near Kurnool, Nandyal, Cuddapah, Junction with NH 4 near Chittoor]).
route(NH18A,[Puthalapattu, Tirupati]).
route(NH19,[Ghazipur, Balia, Patna]).
route(NH20,[Pathankot, Mandi]).
route(NH20A,[Junction with NH 20 in Nagrota, Ranital, Dehra, Junction with NH 70 in Mubarikpur]).
route(NH21,[Junction with NH 22 near Chandigarh, Ropar, Bilaspur, Mandi, Kullu, Manali]).
route(NH21A,[Pinjore, Nalagarh, Swarghat]).
route(NH22,[Ambala, Kalka, Shimla, Narkanda, Rampur, Indo China Border near Shipkila]).
route(NH23,[Chas, Bokaro, Ranchi, Rourkela, Talcher, Junction with NH 42]).
route(NH24,[Delhi, Moradabad, Bareilly, Lucknow]).
route(NH24A,[Bakshi Ka Talab, Chenhat (NH 28)]).
route(NH24B,[Lucknow, Raebareli, Unchahar, Allahabad]).
route(NH25,[Lucknow, Unnao, Kanpur Barah, Jhansi, Shivpuri]).
route(NH25A,[Km 19 (NH 25), Bakshi Ka Talab]).
route(NH26,[Jhansi, Lakhnadon]).
route(NH26A,[Junction NH 26 in Sagar, Jeruwakhera, Khurai, Bina]).
route(NH26B,[Junction NH 26 in Narsinghpur, Amarwara, Chhindwara, Savner via NH,69]).
route(NH27,[Allahabad, Mangawan]).
route(NH28,[Junction with NH 31 Near Barauni, Muzaffarpur, Pipra, Kothi, Gorakhpur, Lucknow]).
route(NH28A,[Junction with NH 28 near Pipra, Kothi, Sagauli, Raxaul, Indo,Nepal Border]).
route(NH28B,[Junction with NH 28A at Chhapra, Bettiah, Lauriya, Bagaha, Junction with NH 28 near Kushinagar]).
route(NH28C,[Junction with NH 28 nearBarabanki, Bahraich, Nepalganj]).
route(NH29,[Varanasi, Ghazipur, Gorakhpur, Pharenda, Sunali]).
route(NH30,[Junction with NH 2 near Mohania, Patna, Bakhtiarpur]).
route(NH30A,[Fatuha, Chandi, Harnaut, Barh]).
route(NH31,[Junction with NH 2 near Barhi, Bakhtiarpur, Mokameh, Purnea,  Dalkhola, Siliguri, Sevok, Cooch Behar, North Salmara, Nalbari, Charali, Amingaon Junction with NH 37]).
route(NH31A,[Sevok, Gangtok]).
route(NH31B,[North Salmara, Junction with NH 37 near Jogighopa]).
route(NH31C,[Near Galgalia, Bagdogra, Chalsa, Nagrakata, Goyerkata, Dalgaon, Hasimara, Rajabhat Khawa, Kochgaon, Sidili, Junction with NH 31 near Bijni]).
route(NH31D,[Junction with NH 31 near Siliguri, Fulbari, Mainaguri, Dhupguri Falakata, Sonapur, Junction with NH 31C near Salsalabari]).
route(NH32,[Junction with NH 2 near Gobindpur, Dhanbad, Chas, Jamshedpur]).
route(NH33,[Junction with NH 2 near Barhi, Ranchi, Jamshedpur  Junction with NH 6 near Baharagora]).
route(NH34,[Junction with NH 31 near Dalkhola, Baharampur, Barasat, Dum Dum]).
route(NH35,[Barasat, Bangaon, Petrapole on India–Bangladesh border]).
route(NH36,[Nowgong, Dimapur (Manipur Road)]).
route(NH37,[Junction with NH 31B near Goalpara, Guwahati, Jorabat, Kamargaon, Makum, Saikhoaghat, Roing]).
route(NH37A,[Kuarital, Junction with NH 52 near Tezpur]).
route(NH38,[Makum, Ledo, Lekhapani]).
route(NH39,[Numaligarh, Imphal, Pallel, Indo,Burma border]).
route(NH40,[Jorabat, Shillong, Bangladesh–India border near Dawki, Jowai]).
route(NH41,[Junction with NH 6 near Kolaghat, Tamluk, Haldia Port]).
route(NH42,[Junction with NH 6 Sambalpur Angul Junction with NH 5 near Cuttack]).
route(NH43,[Raipur, Jagdalpur, Vizianagaram Junction with NH 5 near Natavalasa]).
route(NH44,[Nongstoin, Shillong, Passi, Badarpur, Agartala, Sabroom]).
route(NH44A,[Aizawl, Manu]).
route(NH45,[Chennai, Tambaram, Tindivanam, Villupuram, Trichy, Manapparai, Dindigul, Periyakulam, Junction with NH 49 near Theni]).
route(NH45A,[Villupuram, Pondicherry, Chidambaram, Nagapattinam]).
route(NH45B,[Trichy, Viralimalai, Melur, Madurai, Tuticorin]).
route(NH45C,[Junction with NH 67 near Thanjavur, Kumbakonam, Vriddhachalam, NH,45 near Ulundurpettai]).
route(NH46,[Krishnagiri, Ranipet, Walajapet]).
route(NH47,[Salem, Sankagiri, Chithode, Perundurai(Erode bypass), Perumanallur, Avinashi, Coimbatore, Palghat, Trichur, Kochi, Alappuzha, Quilon, Trivandrum, Nagercoil, Kanyakumari]).
route(NH47A,[Junction with NH 47 at Kundanoor, Willington Island in Kochi]).
route(NH47B,[Junction with NH 47 at Nagercoil, Junction with NH 7 near Kavalkinaru]).
route(NH47C,[Junction with NH 47 at Kalamassery, Vallarpadom ICTT in Kochi]).
route(NH48,[Bangalore, Hassan, Mangalore]).
route(NH49,[Kochi, Madurai, Dhanushkodi]).
route(NH50,[Nashik, Junction with NH 4 near Pune]).
route(NH51,[Paikan, Tura, Dalu]).
route(NH52,[Baihata, Charali, Tezpur, Banderdewa – North Lakhimpur, Pasighat, Tezu, Sitapani Junction with NH 37 near Saikhoaghat]).
route(NH52A,[Banderdewa, Itanagar, Gohpur]).
route(NH52B,[Kulajan, Dibrugarh]).
route(NH53,[Junction with NH 44 near Badarpur, Jirighat, Silchar, Imphal]).
route(NH54,[Dabaka, Lumding, Silchar, Aizawl, Tuipang]).
route(NH54A,[Theriat, Lunglei]).
route(NH54B,[Venus Saddle, Saiha]).
route(NH55,[Siliguri, Darjeeling]).
route(NH56,[Lucknow, Varanasi]).
route(NH56A,[Chenhat(NH 28), Km16(NH 56)]).
route(NH56B,[Km15(NH 56), km 6(NH 25]).
route(NH57,[Muzaffarpur, Darbhanga, Forbesganj, Purnea]).
route(NH57A,[Junction of NH 57 near Forbesganj, Jogbani]).
route(NH58,[Delhi, Ghaziabad, Meerut, Haridwar, Badrinath, Mana Pass]).
route(NH59,[Ahmedabad, Godhra, Dhar, Indore, Raipur, Nuapada, Kharial, Baliguda, Surada, Asika, Hinjilicut, Brahmapur, Gopalpur,on,Sea]).
route(NH59A,[Indore, Betul]).
route(NH60,[Balasore, Kharagpur — Raniganj,  Siuri, Moregram (junction at NH 34 )]).
route(NH61,[Kohima, Wokha, Mokokchung, Jhanji]).
route(NH62,[Damra, Baghmara, Dalu]).
route(NH63,[Ankola, Hubli, Hospet, Gooty]).
route(NH64,[Chandigarh, Rajpura, Patiala, Sangrur, Bhatinda, Dabwali]).
route(NH65,[Ambala, Kaithal, Hissar, Fatehpur, Jodhpur, Pali]).
route(NH66,[Pondicherry, Tindivanam, Gingee, Thiruvannamalai, Krishnagiri]).
route(NH67,[Nagapattinam, Tiruchirapalli, Karur, Palladam, Coimbatore, Mettupalayam, Coonoor, Ooty, Gundlupet]).
route(NH68,[Ulundrupet, Chinnasalem, Kallakkurichchi, Attur, vazhapadi, Salem]).
route(NH69,[Nagpur, Obedullaganj]).
route(NH69A,[Multai, Seoni]).
route(NH70,[Jalandhar, Hoshiarpur, Hamirpur, Dharmapur, Mandi]).
route(NH71,[Jalandhar, Moga, Sangrur,Jind, Rohtak, Rewari, Bawal]).
route(NH71A,[Rohtak, Gohana, Panipat]).
route(NH71B,[Rewari, Dharuhera, Taoru, Sohna, Palwal]).
route(NH72,[Ambala, Nahan, Paonta Sahib, Dehradun, Haridwar]).
route(NH72A,[Chhutmalpur, Biharigarh, Dehradun]).
route(NH73,[Roorkee, Saharanpur, Yamuna Nagar, Saha, Panchkula]).
route(NH74,[Haridwar, Nagina, Kashipur, Kichha, Pilibhit, Bareilly]).
route(NH75,[Gwalior, Jhansi, Chhatarpur, Rewa, Renukut, Garhwa, Daltonganj, Ranchi]).
route(NH76,[Pindwara, Udaipur, Mangalwar, Kota, Shivpuri, Jhansi, Banda, Allahabad]).
route(NH77,[Hajipur, Sitamarhi, Sonbarsa]).
route(NH78,[Katni, Shahdol, Surajpur, Jashpurnagar, Gumla]).
route(NH79,[Ajmer, Nasirabad, Neemuch, Mandsaur, Indore]).
route(NH79A,[Kishangarh(NH 8), Nasirabad(NH 79)]).
route(NH80,[Mokameh, Rajmahal, Farrakka]).
route(NH81,[Kora, Katihar, Malda]).
route(NH82,[Gaya, Bihar Sharif, Mokameh]).
route(NH83,[Patna, Jahanabad, Gaya, Bodhgaya, Dhobi]).
route(NH84,[Arrah, Buxar]).
route(NH85,[Chappra, Gopalganj]).
route(NH86,[Kanpur, Ramaipur, Ghatampur, Chhatarpur, Sagar, Bhopal, Dewas]).
route(NH87,[Rampur, Pantnagar, Haldwani, Nainital]).
route(NH88,[Shimla, Bilaspur, Hamirpur, Nadaun, Kangra, Himachal Pradesh, Mataur, Himachal Pradash, NH 20]).
route(NH90,[Baran, Aklera]).
route(NH91,[Ghaziabad, Aligarh, Eta, Kannauj, Bilhaur, Shivrajpur, Chobepur, Mandhana, Kalianpur, Rawatpur, Kanpur]).
route(NH92,[Bhongaon, Etawah, Gwalior]).
route(NH93,[Agra, Aligarh, Babrala, Chandausi, Moradabad]).
route(NH94,[Rishikesh, Ampata, Tehri, Dharasu, Kuthanur, Yamunotri]).
route(NH95,[Kharar(Chandigarh), Ludhiana, Jagraon, Ferozepur]).
route(NH96,[Faizabad, Sultanpur, Pratapgarh, Allahabad]).
route(NH97,[Ghazipur, Zamania, Saiyedraja]).
route(NH98,[Patna, Aurangabad, Rajhara]).
route(NH99,[Dobhi, Chatra, Chandwa]).
route(NH100,[Chatra, Hazaribagh, Bagodar]).
route(NH101,[Chhapra, Baniapur, Mohammadpur]).
route(NH102,[Chhapra, Rewaghat, Muzaffarpur]).
route(NH103,[Hajipur, Mushrigharari]).
route(NH104,[Chakia, Sitamarhi, Jainagar, Narahia]).
route(NH105,[Darbhanga, Aunsi, Jainagar]).
route(NH106,[Birpur, pipra, Madhepura, Bihpur]).
route(NH107,[Maheshkhunt, Sonbarsa Raj, Simri,Bakhtiarpur, Bariahi, Saharsa, Madhepura, Purnea]).
route(NH108,[Dharasu, Uttarkashi, Yamunotri, Gangotri Dham]).
route(NH109,[Rudraprayag, Guptkashi, Kedarnath Dham]).
route(NH110,[Junction with NH 98, Arwal, Jehanabad, Bandhuganj, Kako, Ekangarsarai Bihar Sharif, Junction with	NH 31]).
route(NH111,[Bilaspur, Katghora, Ambikapur,Surajpur on NH 78]).
route(NH112,[Bar Jaitaran, Bilara, Kaparda, Jodhpur, Kalyanpur, Pachpadra, Balotra, Tilwara, Bagundi, Dhudhwa, Madhasar, Barmer]).
route(NH113,[Nimbahera, Bari, Pratapgarh, Zalod, Dahod]).
route(NH114,[Jodhpur, Balesar, Dachhu, Pokaran]).
route(NH116,[Tonk, Uniara, Sawai Madhopur]).
route(NH117,[Haora, Bakkhali]).
route(NH119,[Pauri, Najibabad, Meerut]).
route(NH121,[Kashipur, Bubakhal]).
route(NH123,[Barkot, Vikasnagar]).
route(NH125,[Sitarganj, Pithorgarh]).
route(NH150,[Aizawl, Churachandpur, Imphal, Ukhrul, Jessami, Kohima]).
route(NH151,[Karimganj, Indo,Bangladesh border]).
route(NH152,[Patacharkuchi, Bhutan,India Border]).
route(NH153,[Ledo, Lekhapani, Indo,Myanmar border]).
route(NH154,[Dhaleswar, Bairabi, Kanpui]).
route(NH155,[Tuensang, Shamator, Meluri, Kiphire, Pfutsero]).
route(NH157,[Kanpur, Raebareli, Sultanpur, Azamgarh, Siwan, Muzaffarpur]).
route(NH200,[Chhapra, Siwan, Gopalganj]).
route(NH201,[Borigumma, Bolangir, Bargarh]).
route(NH202,[Hyderabad – Warangal, Venkatapuram, Bhopalpatnam]).
route(NH203,[Bhubaneswar, Puri]).
route(NH204,[Ratnagiri, Pali, Sakharpa, Malakapur, Shahuwadi, Kolhapur, Sangli, Pandharpur, Solapur, Tuljapur, Latur, Nanded, Yavatmal, Wardha, Nagpur]).
route(NH205,[Anantpur, Renigunta, Chennai]).
route(NH206,[Tumkur, Shimoga, Honnavar]).
route(NH207,[Hosur, Bagalur, Sarjapur, Hoskote, Devanhalli, Doddaballapura,Dobbaspet]).
route(NH208,[Kollam, kundara, kottarakkara, Punalur, Thenmala,Aryankavu, Sengottai, Tenkasi, Rajapalayam, Thirumangalam(Madurai)]).
route(NH209,[Dindigul, Palani, Pollachi, Coimbatore, Punjai Puliampatti, Sathyamangalam, Chamrajnagar, Kollegal, Malavalli, Kanakapura, Bangalore]).
route(NH210,[Trichy, Pudukottai, Karaikudi, Devakottai, Ramanathapuram]).
route(NH211,[Solapur, Osmanabad, Aurangabad, Dhule]).
route(NH212,[Kozhikode, Mysore, Kollegal]).
route(NH213,[Palghat, Kozhikode]).
route(NH214,[Kathipudi, Kakinada, Pamarru]).
route(NH214A,[Digamarru, Narsapur, Machilipatnam, Challapalle, Avanigadda, Repalle, Bapatla, Chirala, Ongole]).
route(NH215,[Panikoili, Keonjhar, Rajamunda]).
route(NH216,[Raigarh, Sarangarh, Saraipali]).
route(NH217,[Raipur, Asika]).
route(NH218,[Bijapur, Hubli]).
route(NH219,[Madanapalle, Kuppam, Krishnagiri]).
route(NH220,[Kollam, Kottarakkara, Adoor, Kottayam, Pampady, Ponkunnam, Kanjirappalli, Mundakayam, Peermade, Vandiperiyar, Kumily, Theni]).
route(NH221,[Vijayawada, Bhadrachalam, Jagdalpur]).
route(NH222,[Kalyan, Murbad, Aalephata, Ahmednagar, Tisgaon, Pathardi,Yeli, Kharwandi, Padalsingi, Majalgaon, Pathri,  Manwat, Parbhani, Nanded,Bhokar, Bhisa, Nirmal]).
route(NH223,[Port Blair, Baratang, Mayabunder]).
route(NH224,[Khordha, Nayagarh, Sonapur, Balangir]).
route(NH226,[Perambalur, Pudukkottai, Sivagangai, Manamadurai]).
route(NH227,[Trichy, Chidambaram]).
route(NH228,[Sabarmati Ashram, Nadiad, Anand, Surat, Navsari,Dandi]).
route(NH229,[Tawang, Pasighat]).
route(NH230,[Madurai, Sivagangai, Thondi]).
route(NH231,[Raibareli, Jaunpur]).
route(NH232,[Ambedkarnagar (Tanda), Banda]).
route(NH232A,[Unnao, Lalganj (Junction of NH,232)]).
route(NH233,[India,Nepal border, Varanasi Azamgarh]).
route(NH234,[Mangalore, Belthangady, Belur, Huliyar, Sira, Chintamani, Venkatagirikota, Gudiyatham, Katpadi, Vellore, Thiruvannaamalai, Villuppuram [1]]).
route(NH235,[Meerut, Hapur, Gulaothi, Bulandshahr]).
