knows(vismay,malayalam).
knows(arko,bengali).
knows(vismay,english).
knows(arko,english).
knows(arko2,bengali).
knows(vismay2,malayalam).

/* actually the talk and comm should be interchanged
	then only it will make sense */

equal(X,Y) :- X==Y.
nequal(X,Y) :- X\=Y.

comm(X,Y) :- knows(X,Z),knows(Y,Z),nequal(X,Y).

talk(X,Y) :- knows(X,L),knows(Y,L),nequal(X,Y).
talkl(X,Y) :- knows(X,L),knows(Z,L),talk(Z,Y).
/* there is something wrong if we write,
	talk(X,Y) :- talk(X,Z),talk(Z,Y).
	it will go to an infinite loop
	maybe it has something to do with the 
	unificaiton algo. Or maybe it's because
	of that infinite loop that keeps occuron
*/


