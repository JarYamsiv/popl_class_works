/* comments look like this */
/* load using swipl -s name.pl*/

man(socretes).
man(plato).
woman(helen).

mortal(X) :- man(X).
mortal(X) :- woman(X).

ancestor(X,Y) :- father(X,Y).
ancestor(X,Y) :- father(X,Z),ancestor(Z,Y).
ancestor(X,Y) :- mother(X,Y).
ancestor(X,Y) :- mother(X,Z),ancestor(Z,Y).


