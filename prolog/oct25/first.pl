/* ! means true in prolog */

member(X,[X|_]) :- !.
member(X,[_|V]) :- member(X,V).

before(X,Y,[X|B]) :- member(Y,B).
before(X,Y,[_|V]) :- before(X,Y,V).

head([X|B],X) :- !.

tail([Y],Y) :- !.
tail([A|B],Y) :- tail(B,Y).

/* x and y are positions r is an array and T is not used as it has to return the value */

towards(X,Y,R,T) :- before(X,Y,R),tail(R,T).
towards(X,Y,R,T) :- before(Y,X,R),head(R,T).

/*junction will take a point and list of routes and checks whether that point is a junction*/

/*
	we can make a database by
	route(NH53,[...]).
	route(NH47,[...]).
*/

junction(X,[]) :- !.
junction(X,[A|B]) :- member(X,A),junction(X,B).
