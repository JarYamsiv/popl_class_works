signature UNIQUE = 
sig
	type uid
	val allocate : unit ->  uid
	val toInt    : uid  ->  int
end

functor Unique (E:sig end):UNIQUE = 
struct
	type uid = int
	val current = ref 0
	fun allocate () = let
		val x = !current
		in
			current :=x+1;
			x (*this does the calculation !current =x+1 and returns x*)
		end

	fun toInt x =x
end

structure empty =  struct end;

structure Uid = Unique(empty);

datatype 'a monotype = BOOL
						|ARROW of 'a monotype * 'a monotype
						|TVAR of 'a;

datatype expr = 
	V of string 
	| A of expr * expr
	| L of string * expr;

datatype 'v VAR = UserVar of 'v
				| Temp of Uid.uid;

fun join (TVAR x) = x
		| join (ARROW(x,y)) = ARROW(join x,join y)
		| join BOOL = BOOL;  