signature UNIQUE = 
sig
	type uid
	val allocate : unit ->  uid
	val toInt    : uid  ->  int
end

functor Unique (E:sig end):UNIQUE = 
struct
	type uid = int
	val current = ref 0
	fun allocate () = let
		val x = !current
		in
			current :=x+1;
			x (*this does the calculation !current =x+1 and returns x*)
		end

	fun toInt x =x
end

structure empty =  struct end;

structure U = Unique(empty);
structure V = Unique(empty);
(* if we have one more structure V = Unique(empty) then they are of same type but they are different like val a=6; val b=7 *)
val u1 = U.allocate()
val u2 = V.allocate()
val u3 = U.allocate()
val u1_int = U.toInt(u1);
val u2_int = U.toInt(u2);
val y3_int = U.toInt(u3);
