(*second class*)

(* curry*)
fun curry f=fn x=>fn y=>f(x,y);

(* uncurry *)
fun uncurry f (x,y) = f x y;

(* add function in curry mode *)
fun curryAdd x y=x+y;

(* add function in uncurry mode *)
fun uncurryAdd (x,y) =  x+y;






