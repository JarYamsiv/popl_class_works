val x = ref 0;
!x;
x:=5;

(*counter*)

signature COUNTER = 
sig
	val inc:  int -> unit
	val show: unit -> int
end

structure counter:COUNTER = 
struct
	val     x = ref 0;
	fun inc y = x:= !x + y ; 
	fun show () = !x;
end