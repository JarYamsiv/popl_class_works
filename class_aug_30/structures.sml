(* class works: aug-30 *)


(*first structure*)
structure A = 
struct 
	val x = 42;
end


(*tree*)
structure tree = 
struct 
	datatype 'a t = 
		null 
		| node of 'a t* 'a * 'a t;

	fun map f (node(l,a,r)) = node(map f l,f a,map f r)
		|map f null  = null;
end

(*opening is like  using namespace *)
open A;
val a =x;


signature NOFUNNYNAME = 
sig
	val y:int;
end

structure B:NOFUNNYNAME = 
struct 
	val somefunnyname = 42;
	val y = 52;
end

functor increamentBy (S:NOFUNNYNAME) = 
struct
	fun inc x = x+S.y;
end

structure IB = increamentBy(B);


functor Iby (val y:int) = 
 struct
 	fun inc x = x+y;
 end 

structure IC = Iby(val y = 3);
structure IC = Iby(B);

