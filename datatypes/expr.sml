(*
		ASSIGNMENT: 1
		NAME	  : Vismay Raj T
		Roll no.  : 111601030

*)

datatype binop = PLUS | MINUS | MUL | DIV
datatype uniop = UNIMINUS | UNIPLUS
datatype expr = const of real | B of binop*expr*expr | U of uniop*expr

fun binopDenot PLUS  (x,y)  = x+y
  | binopDenot MUL   (x,y)  = x*y
  | binopDenot MINUS (x,y)  = x-y
  | binopDenot DIV   (x,y)  = x/y;


fun  uniopDenot UNIMINUS (x) = ~1.0*x
	|uniopDenot UNIPLUS  (x) =  1.0*x;

fun  expD (B(oper,e1,e2)) = binopDenot oper (expD e1,expD e2)
	|expD (U(oper,e))     = uniopDenot oper (expD e)
	|expD (const(x))      = x;


(* teseting out *)
val a = const(2.0);
val b = const(3.0);
(*  c = (a+b)*(a/b)  *)
val c =expD (  B(MUL, B(PLUS , a , b) ,B(DIV,a,b) ) );
