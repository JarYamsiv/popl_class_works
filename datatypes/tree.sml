datatype 'a tree = null | node of 'a tree * 'a * 'a tree;
fun singleton x = node(null,x,null);
fun nodeVal (node(a,b,c)) = b;
fun replaceleft (node(a,b,c)) x = node(x,b,c);
